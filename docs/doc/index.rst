.. (C) 2020 ALbert Mietus, dropjes-licentie: copy & use, and betaal met dropjes naar nuttigheid

#############
YP_SE_HT 2020
#############

In 2020 a new class for *HighTech* and/or *Embedded* (Young) Professional **Software Engineers** is setup. It is
more-or-less based on earlier versions. As both the infra structure and the (technical) projects are outdated; a new
setup is created.

Also the lessons learned to train the a team are incorporated.

.. seealso::

   Just as a reference, the documentation of earlier trainings and similar setups:

   * https://Pathways-Extensions-Training.readthedocs.io (**PET**)
   * https://DDS-Demonstrators.readthedocs.io (DDS-Demonstrators)

   .. warning:: Some changes:

      As bitbucket doesn’t support **mercurial** (``hg``) anymore, *and* we are switched to ``git``, the `source` links
      for `PET <https://pathways-extensions-training.readthedocs.io>`_ on ReadTheDocs point to void. The repro’s
      are saved and will be  converted to git “soon”. But for now, they are hard to use a (sphinx) cheat-pages.
      |BR|
      Similar constructs can be found for many projects/docs, including the `DDS-Demonstrators docs
      <https://DDS-Demonstrators.readthedocs.io>`_

      See :doc:`Changes` for more on that.

Training Goals
##############

This training focus on the **Engineering** part of *Software Engineering*, for :doc:`BigProjects` in an embedded,
HighTech systems. Where up to thousands of programers are engineering one (or a few) product, are working with millions
lines of code in a single repository, and where quality is extremely high. Think about pace-makers; you don’t want to
have a single flaw in the system!

As a relative short training can’t provide years of experience, some “encounters” are simulated. We practice
supersprints (SAFe: `Program Increments <https://www.scaledagileframework.com/program-increment/>`_) by having very
small sprint; we are sometimes on the edge of Kanban, to participate in many planning-sessions, etc.
|BR|
Instead of many teams working concurrently, we use sequencial teams: communication in time is even harder than in space.
We have learned that this is motivating too: make sure your new co-worker in next class can continue; or your work is useless.


Team Pages
##########

Each class, each team-member, is motivated to leave *notes* in the :doc:`TeamPages/index`. A collection of (informal)
notes, ordered by team. They contain *quick tip* on various topics; made by (individual) apprentices. They may be
useful, but may be outdated.
|BR|
This is also a nice *playground* for practicing both (advanced) version-controll (git) and lean documenting (sphinx).


Content
#######

.. Hide some intro & admin pages

.. toctree::
   :hidden:

   Changes
   todolist
   



.. toctree::
   :maxdepth: 3
   :glob:

   BigProjects
   */index
   Scrum

