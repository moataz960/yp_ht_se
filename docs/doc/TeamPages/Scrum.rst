.. _Scrum:

=====
Scrum
=====
:author: Moataz Metwally
:date: 2 Dec 2020

.. image:: scrum_process_afa_5000.jpg
   :width: 600
   :align: center

These team pages contain our findings during the project.

.. toctree::
   :glob:
   :maxdepth: 0

   */index
