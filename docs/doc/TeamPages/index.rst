==========
Team pages
==========
:author: Albert
:date: 23 Dec 2017

.. sidebar:: Concept

   Each team (training-group) get an *own* directory (a subdir of this one), to place notes, store
   know-how, etc. That does not imply they are the only ones who may change those articles; it just
   a convenient place to start.

   So whenever you find out something, that may be interesting for your team, or a future team:
   write a note, an article, and place it in your team-section. Or update an existing one (even of
   another team). In all cases, add you name as author or section-author and update the date

   As a team, you may also decided (*are encouraged!*) to **improve** the general
   documentation. This may/does include collecting “notes” and moving them the a more generic
   place. Either about the training, or about the extension.


This part consist of a lot of ‘notes’. They may be new, maintained or outdated. Also the general
quality rules are lowered. This is un purpose: it is better to have and share a brainwave, then to
have it forgotten. As long a the reader knows it status: by this intro everbody should know!

.. toctree::
   :glob:
   :maxdepth: 2

   Ninja
   Scrum
